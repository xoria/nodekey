import { CipherMethod } from "../types/Method";
import { fromSchema, methods } from "../services/crypto/encryption";
import { Profile } from "../types/Profile";

export function genProfile(): Profile {
  return {
    integrity: {},
    bookmarks: {
      "~": null,
    },
    applications: [],
    aliases: {
      "?": "help",
      ll: "ls -l",
      cc: "clear -c",
      n: "mkapp",
      ns: "mkapp -S",
      nk: "mkapp -kf",
      a: "alias",
      ad: "alias -d",
      al: "alias -l",
      g: "key",
      gf: "key -f",
      s: "key -s",
      sc: "key -sc",
      store: "key -s",
    },
  };
}

export function genMethod(): CipherMethod[] {
  return methods().map((schema) => fromSchema(schema));
}
