import { Settings } from "../types/Settings";
import { methods as hashingMethods } from "../services/crypto/hashing";
import { methods as encryptionMethods } from "../services/crypto/encryption";

export default function construct(): Settings {
  return {
    file_encoding: "utf-8",

    encryption: encryptionMethods(),
    hashing: hashingMethods(),

    char_sets: [
      {
        id: 0,
        name: "a-z",
        characters: "abcdefghijklmnopqrstuvwxyz",
        checked: true,
      },
      {
        id: 1,
        name: "A-Z",
        characters: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        checked: true,
      },
      {
        id: 2,
        name: "0-9",
        characters: "0123456789",
        checked: true,
      },
      {
        id: 3,
        name: "Punctuation",
        characters: ".,-+*!?_():;",
        checked: false,
      },
      {
        id: 4,
        name: "Special characters",
        characters: " <>/'\"[]{}|\\=&^%$#@~`",
        checked: false,
      },
    ],
    char_set_custom: {
      name: "Custom",
      checked: true,
    },
    char_set_collections: [
      {
        name: "Alphanumeric",
        char_set_ids: [0, 1, 2],
      },
      {
        name: "With Punctuation",
        char_set_ids: [0, 1, 2, 3],
      },
      {
        name: "Advanced",
        char_set_ids: [0, 1, 2, 3, 4],
      },
    ],

    default_length: 128,

    output: {
      prompt: { meta: true },
      fetch: {
        meta: true,
        print: false,
        hidden: true,
        clipboards: [
          {
            timeout: 60000,
            boards: ["general", "main", "clipboard", "primary", "ruler"],
          },
        ],
      },
    },

    interactive: {},
  };
}
