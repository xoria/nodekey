import { CommandExecutor } from "../types/CommandExecutor";
import { parseFlags } from "../services/util";
import { getApplications, getData, persist } from "../services/profile";
import { appFromPath, pathFromApp } from "../services/path/application";
import { getState } from "../services/state";
import { printBookmark } from "../io/print";
import {
  errBookmarkAlreadyExists,
  errBookmarkDeleteHomeFail,
  errBookmarkNotFound,
} from "../io/feedback";
import { completeList } from "../services/completion";

const CMD_BOOKMARK = {
  help: {
    head: "[-f] [ID=~] | -d ID... | -l",
    description:
      "Create/Delete some bookmark (with/by ID) or list existing bookmarks. '~' can be used as special ID " +
      "to set a new home marker.",
    see: ["jump"],
  },
  cmd: "bookmark",
  callback: bookmark,
  complete: completeBookmark,
} as CommandExecutor;
const CMD_JUMP = {
  help: {
    head: "[ID=~]",
    description: "Change current application to specified bookmark.",
    see: ["bookmark", "cd"],
  },
  cmd: "jump",
  callback: jump,
  complete: completeJump,
} as CommandExecutor;

export default [CMD_BOOKMARK, CMD_JUMP];

function completeBookmark(currentParam: string, parameters: string[]) {
  if (currentParam.length > 0 && currentParam[0] === "-") {
    return "";
  }
  const flags = parseFlags(parameters);
  if (!Reflect.has(flags.map, "d")) {
    return "";
  }
  return completeList(currentParam, Object.keys(getData().bookmarks));
}

function completeJump(currentParam: string, parameters: string[], idx: number) {
  if (
    parameters.length === 0 ||
    (parameters.length === 1 && idx < parameters.length)
  ) {
    return completeList(currentParam, Object.keys(getData().bookmarks));
  }
  return "";
}

async function bookmark(parameters: string[]) {
  const flags = parseFlags(parameters);
  const marks = getData().bookmarks;
  let pendingPersist = false;
  if (Reflect.has(flags.map, "d")) {
    for (const id of flags.remainder) {
      if (id === "~") {
        errBookmarkDeleteHomeFail();
        break;
      }
      if (Reflect.has(marks, id)) {
        Reflect.deleteProperty(marks, id);
        pendingPersist = true;
      }
    }
  } else if (Reflect.has(flags.map, "l")) {
    for (const mark in marks) {
      const applications = getApplications();
      const appId = marks[mark];
      printBookmark(
        mark,
        pathFromApp(appId === null ? null : applications[appId]).toString(true)
      );
    }
  } else {
    const id = flags.remainder.length === 0 ? "~" : flags.remainder[0];
    const force = Reflect.has(flags.map, "f");
    if (Reflect.has(marks, id) && !force) {
      return errBookmarkAlreadyExists(id);
    }
    const app = appFromPath(getState().cwd);
    marks[id] = app && app._id;
    pendingPersist = true;
  }
  pendingPersist && (await persist());
}

function jump(parameters: string[]) {
  const id = parameters.length === 0 ? "~" : parameters[0];
  const marks = getData().bookmarks;
  if (!Reflect.has(marks, id)) {
    return errBookmarkNotFound(id);
  }
  const appId = marks[id];
  getState().cwd = pathFromApp(
    appId === null ? null : getApplications()[appId]
  );
}
