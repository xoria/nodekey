import CMD_REMOVE from "./remove";
import CMD_META from "./meta";
import CMD_MOVE from "./move";

export default [CMD_REMOVE, CMD_META, CMD_MOVE];
