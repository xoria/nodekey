import { constants as FS, promises as fs } from "fs";
import { join } from "path";
import { mapKeys } from "lodash";
import {
  CipherMethod,
  CipherMethodDTO,
  cipherMethodFromDTO,
  cipherMethodToDTO,
} from "../types/Method";
import { decrypt, encrypt } from "./crypto/index";
import { genMethod, genProfile } from "../constants/defaultProfile";
import { profileFromDTO, Profile, profileToDTO } from "../types/Profile";
import { terminal } from "terminal-kit";
import { EOL } from "os";
import { cleanUp, textInput } from "../io/terminal";
import { Application } from "../types/Application";
import CommandError from "../types/CommandError";

let directory: string;
let profile: Profile;
let rootApplications: { [name: string]: Application };
let rootApplicationsList: Application[];
let applications: { [_id: string]: Application };
let fileKey: Buffer | null;

export async function init(dir: string): Promise<void> {
  directory = dir;
  const profileMethodFile = join(dir, "profile.method");
  try {
    await fs.access(profileMethodFile, FS.R_OK);
  } catch (ignored) {
    await fs.writeFile(
      profileMethodFile,
      JSON.stringify(genMethod().map(cipherMethodToDTO))
    );
  }
}

export async function read(dir: string = directory, key?: string) {
  let profile: Profile | null;
  if (key !== void 0) {
    profile = await readFile(dir, key ? Buffer.from(key) : null);
  } else {
    profile = await promptKey(dir);
  }
  if (profile === null) {
    throw new CommandError("Login failed.");
  }
}

async function promptKey(dir: string = directory): Promise<Profile | null> {
  if (await exists(dir)) {
    let readResult: Profile | null;
    let i = 0;
    do {
      if (i > 0) {
        terminal.red.bold("Login failed. Try again.")(EOL);
      }
      terminal("Insert profile password: ");
      const key = await textInput({ echo: false });
      readResult = await readFile(dir, key ? Buffer.from(key) : null);
    } while (readResult === null && ++i < 3);
    return readResult;
  } else {
    let i = 0;
    let key: string | null;
    do {
      if (i > 0) {
        terminal.red.bold("Mismatch. Try again.")(EOL);
      }
      terminal("Specify new profile password: ");
      key = await textInput({ echo: false });
      terminal("Confirm new profile password: ");
      const confirmKey = await textInput({ echo: false });
      if (confirmKey !== key) {
        key = null;
        if (++i === 3) {
          terminal.error.red("Password specification failed. Exiting.")(EOL);
          await cleanUp(4);
        }
      }
    } while (key === null);
    return await readFile(dir, key ? Buffer.from(key) : null);
  }
}

export function getData() {
  return profile;
}

export function getRootApplications() {
  return rootApplications;
}

export function getRootApplicationsList() {
  return rootApplicationsList;
}

export function getApplications() {
  return applications;
}

export function getKey() {
  return fileKey;
}

export async function exists(dir: string = directory): Promise<boolean> {
  try {
    await fs.access(profileFile(dir), FS.R_OK);
  } catch (ignored) {
    return false;
  }
  return true;
}

async function readFile(
  dir: string = directory,
  password: Buffer | null = fileKey
): Promise<Profile | null> {
  fileKey = password;
  // start IO requests
  const profilePromise = fs.readFile(profileFile(dir));
  const methodsPromise = readMethod(dir);
  // await IO requests with early return if no profile file exists
  let encrypted: Buffer;
  const methods = await methodsPromise;
  try {
    encrypted = await profilePromise;
  } catch (ignored) {
    profile = genProfile();
    rootApplicationsList = profile.applications.filter(
      (app) => app.parent === null
    );
    rootApplications = mapKeys(rootApplicationsList, "name");
    applications = mapKeys(profile.applications, "_id");
    await persist(dir);
    return profile;
  }
  // decrypt profile according to methods
  let decrypted: Buffer;
  if (methods.length === 0) {
    decrypted = encrypted;
  } else {
    if (password === null) {
      throw new Error("Profile cannot be decrypted without key.");
    }
    const result: Buffer | null = await decrypt(encrypted, methods, password);
    if (result === null) {
      return null;
    }
    decrypted = result;
  }
  // parse and store profile state
  profile = profileFromDTO(JSON.parse(decrypted.toString()));
  applications = mapKeys(profile.applications, "_id");
  rootApplicationsList = profile.applications.filter(
    (it) => it.parent === null
  );
  rootApplications = mapKeys(rootApplicationsList, (it) => it.name);
  return profile;
}

export async function persist(
  dir: string = directory,
  password: Buffer | null = fileKey
): Promise<void> {
  fileKey = password;
  const methods = await readMethod(dir);
  let buffer = Buffer.from(JSON.stringify(profileToDTO(profile)));
  if (methods.length > 0) {
    if (password === null) {
      throw new Error("Profile cannot be encrypted without key.");
    }
    buffer = await encrypt(buffer, methods, password);
  }
  return await fs.writeFile(profileFile(dir), buffer);
}

async function readMethod(dir: string = directory): Promise<CipherMethod[]> {
  return await fs
    .readFile(methodFile(dir))
    .then(
      (content): CipherMethodDTO[] =>
        JSON.parse(content.toString()) as CipherMethodDTO[]
    )
    .then((it) => it.map(cipherMethodFromDTO));
}

function profileFile(dir: string) {
  return join(dir, "profile");
}

function methodFile(dir: string) {
  return join(dir, "profile.method");
}
