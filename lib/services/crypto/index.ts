import {
  Cipher,
  createCipheriv,
  createDecipheriv,
  createHmac,
  Decipher,
  scrypt,
} from "crypto";
import { CipherMethod, HashMethod, SCryptOptions } from "../../types/Method";
import { KeySchema } from "../../types/Application";
import CommandError from "../../types/CommandError";

const RECYCLE_HMAC_ALGORITHM = "sha512";

interface HashAnalytics {
  bitsPerCharacter: number;
  minRequiredBytes: number;
  successProbability: number;
}

export async function hash(
  methods: HashMethod[],
  phrase: Buffer | null,
  password: Buffer,
  optimalSize = 32
): Promise<Buffer> {
  if (methods.length === 0) {
    throw new Error("No methods passed.");
  }
  const buffer = await applyHmac(methods[0], phrase, password, optimalSize);
  for (let i = 1; i < methods.length; i++) {
    phrase = await applyHmac(methods[i], buffer, password, optimalSize);
  }
  return buffer;
}

export async function hashKey(
  methods: HashMethod[],
  schema: KeySchema,
  password: Buffer
): Promise<Buffer> {
  if (methods.length === 0) {
    throw new Error("No methods passed.");
  }
  const charsetSize = schema.characters.length;
  if (charsetSize === 0) {
    throw new CommandError(
      "Key generation cannot be performed without a charset."
    );
  }
  const analytics = getHashAnalytics(schema);
  let buffer = await applyHmac(methods[0], null, password, analytics);
  for (let i = 1; i < methods.length; i++) {
    buffer = await applyHmac(methods[i], buffer, password, analytics);
  }
  let result = "";
  let currentValue = 0,
    currentBits = 0;
  // eslint-disable-next-line no-constant-condition
  while (true) {
    // iterate over the buffer that contains the entropy
    for (let byteIdx = 0; byteIdx < buffer.length; byteIdx++) {
      let byteValue = buffer.readInt8(byteIdx);
      for (let bitIdx = 0; bitIdx < 8; bitIdx++) {
        currentValue = (currentValue << 1) | (byteValue & 1);
        byteValue >>= 1;
        if (++currentBits === analytics.bitsPerCharacter) {
          if (currentValue < charsetSize) {
            result += schema.characters[currentValue];
            if (result.length === schema.length) {
              return Buffer.from(result); // all work done
            }
          }
          // drop value if >= charsetSize. No circumvention would be possible that keeps linear distribution. Since the
          // next hash cycle is fed the complete current buffer, the entropy is recycled (insignificant loss).
          currentValue = currentBits = 0;
        }
      }
    }
    // additional hmac cycle to re-mix dropped entropy
    buffer = recycle(methods, buffer, password);
  }
}

function recycle(methods: HashMethod[], buffer: Buffer, password: Buffer) {
  const hash = createHmac(RECYCLE_HMAC_ALGORITHM, password);
  hash.update(buffer);
  for (const method of methods) {
    hash.update(method.salt);
  }
  return hash.digest();
}

export async function decrypt(
  phrase: Buffer,
  methods: CipherMethod[],
  password: Buffer
): Promise<Buffer | null> {
  for (let i = methods.length - 1; i >= 0; i--) {
    const result = await _decrypt(phrase, methods[i], password);
    if (result === null) {
      return null;
    }
    phrase = result;
  }
  return phrase;
}

async function _decrypt(
  phrase: Buffer,
  method: CipherMethod,
  password: Buffer
): Promise<Buffer | null> {
  const decipher = await getDecipher(method, password);
  const buf0 = decipher.update(phrase);
  try {
    const buf1 = decipher.final();
    return Buffer.concat([buf0, buf1], buf0.length + buf1.length);
  } catch (e) {
    return null; // decryption failed (probably wrong password)
  }
}

export async function encrypt(
  phrase: Buffer,
  methods: CipherMethod[],
  password: Buffer
): Promise<Buffer> {
  for (const method of methods) {
    phrase = await _encrypt(phrase, method, password);
  }
  return phrase;
}

async function _encrypt(
  phrase: Buffer,
  method: CipherMethod,
  password: Buffer
): Promise<Buffer> {
  const cipher = await getCipher(method, password);
  const buf0 = cipher.update(phrase);
  const buf1 = cipher.final();
  return Buffer.concat([buf0, buf1], buf0.length + buf1.length);
}

async function getDecipher(
  method: CipherMethod,
  password: Buffer
): Promise<Decipher> {
  const key = await getKey(
    method.salt,
    password,
    method.algorithm.keyBytes,
    method.keyOptions
  );
  return createDecipheriv(
    method.algorithm.name,
    key,
    method.iv,
    method.options
  );
}

async function getCipher(
  method: CipherMethod,
  password: Buffer
): Promise<Cipher> {
  const key = await getKey(
    method.salt,
    password,
    method.algorithm.keyBytes,
    method.keyOptions
  );
  return createCipheriv(method.algorithm.name, key, method.iv, method.options);
}

async function applyHmac(
  method: HashMethod,
  phrase: Buffer | null,
  password: Buffer,
  analytics: HashAnalytics | number
): Promise<Buffer> {
  switch (method.algorithm.name) {
    case "scrypt":
      // eslint-disable-next-line no-case-declarations
      let initialBytes: number;
      if (typeof analytics === "number") {
        initialBytes = analytics;
      } else {
        initialBytes = Math.ceil(
          analytics.minRequiredBytes / analytics.successProbability
        );
      }
      return await getKey(
        phrase === null
          ? method.salt
          : Buffer.concat(
              [phrase, method.salt],
              phrase.length + method.salt.length
            ),
        password,
        initialBytes,
        method.options
      );
    default:
      // eslint-disable-next-line no-case-declarations
      const hmac = createHmac(method.algorithm.name, password);
      if (phrase !== null) {
        hmac.update(phrase);
      }
      hmac.update(method.salt);
      return hmac.digest();
  }
}

async function getKey(
  salt: Buffer,
  password: Buffer,
  size: number,
  options: SCryptOptions = {}
): Promise<Buffer> {
  return await new Promise((resolve: (value: Buffer) => void, reject) => {
    scrypt(password, salt, size, options, (err, key) =>
      err ? reject(err) : resolve(key)
    );
  });
}

function getHashAnalytics(schema: KeySchema): HashAnalytics {
  const charsetSize = schema.characters.length;
  let bpc = 1; // bits per character
  let pow2 = 2;
  while (pow2 < charsetSize) {
    bpc++;
    pow2 *= 2;
  }
  return {
    bitsPerCharacter: bpc,
    minRequiredBytes: Math.ceil((charsetSize / 256) * schema.length),
    successProbability: pow2 / charsetSize,
  };
}
