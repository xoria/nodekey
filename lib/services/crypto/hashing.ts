import { createHash, getHashes, Hash, randomBytes } from "crypto";
import {
  CipherMethod,
  HashMethod,
  HashMethodSchema,
  cipherMethodToDTO,
  hashMethodToDTO,
} from "../../types/Method";
import { Key } from "../../types/Key";
import { Application } from "../../types/Application";
import { Meta } from "../../types/Meta";

const INTEGRITY_HASH_ALGORITHM = "sha256";

export function methods() {
  const availableHashes = getHashes();
  const methods: HashMethodSchema[] = [];
  if (availableHashes.includes("sha512")) {
    methods.push({ algorithm: { name: "sha512" }, saltBytes: 24 });
  } else if (availableHashes.includes("sha256")) {
    methods.push({ algorithm: { name: "sha256" }, saltBytes: 24 });
  }
  methods.push({
    algorithm: { name: "scrypt" },
    saltBytes: 24,
    options: {
      cost: 65536,
      blockSize: 16,
      parallelization: 2,
      maxmem: 256 * 1024 * 1024,
    },
  });
  return methods;
}

export function fromSchema(
  schema: HashMethodSchema,
  salt: Buffer = randomBytes(schema.saltBytes)
): HashMethod {
  return {
    algorithm: schema.algorithm,
    salt,
    options: schema.options,
  };
}

export function integrityHashKey(key: Key): Buffer {
  const hash = createHash(INTEGRITY_HASH_ALGORITHM);
  hash.update(key.name);
  if (key.hashed) {
    for (const method of key.methods as HashMethod[]) {
      hash.update(JSON.stringify(hashMethodToDTO(method)));
    }
  } else {
    hash.update(key.value as Buffer);
    for (const method of key.methods as CipherMethod[]) {
      hash.update(JSON.stringify(cipherMethodToDTO(method)));
    }
  }
  applyMetaIntegrity(hash, key.meta);
  return hash.digest();
}

export function integrityHashApp(app: Application): Buffer {
  const hash = createHash(INTEGRITY_HASH_ALGORITHM);
  hash.update(app.name);
  if (app.defaultKeyId !== null) {
    const defaultKey = app.keysList.find(
      (key) => key._id === app.defaultKeyId
    ) as Key;
    hash.update(defaultKey.name);
  }
  applyMetaIntegrity(hash, app.meta);
  if (app.schema !== null) {
    hash.update("" + app.schema.length);
    hash.update(app.schema.characters);
  }
  return hash.digest();
}

export function integrityHash(input: Buffer[]): Buffer {
  const hash = createHash(INTEGRITY_HASH_ALGORITHM);
  for (const h of input) {
    hash.update(h);
  }
  return hash.digest();
}

function applyMetaIntegrity(hash: Hash, meta: Meta[]) {
  for (const m of meta) {
    hash.update(m.name);
    hash.update(m.value);
  }
}
