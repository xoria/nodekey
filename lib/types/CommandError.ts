export default class CommandError extends Error {
  cont: boolean;

  constructor(msg: string, cont = false) {
    super(msg);
    this.name = "CMD_ERR";
    this.cont = cont;
  }
}
